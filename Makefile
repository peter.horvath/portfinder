#!/usr/bin/env -S make
.PHONY: help
help:
	cat README.md

nmap-ports:
	xz -d - <saved-nmap-ports.xz >nmap-ports~
	mv -v nmap-ports~ nmap-ports

used-ports: nmap-ports
	grep -v '^#' <nmap-ports|\
	awk '{print $$2}'|\
	sed 's/\/.*//g'|\
	sort -n|\
	uniq >used-ports~
	mv -v used-ports~ used-ports

all-ports:
	seq 1025 65534 >all-ports~
	mv -v all-ports~ all-ports

free-ports: used-ports all-ports
	setop all-ports not used-ports | sort -n >free-ports~
	mv -v free-ports~ free-ports

.PHONY: download-nmap-ports
download-nmap-ports:
	wget -O - https://svn.nmap.org/nmap/nmap-services|xz -9ve - >saved-nmap-ports.xz

pyenv/.env:
	python3 -m venv pyenv
	touch $@

pyenv/.req: pyenv/.env
	. ./pyenv/bin/activate && python3 -m pip install python-Levenshtein
	touch $@

.PHONY: levenshtein
levenshtein:
	$(MAKE) free-ports pyenv/.req
	. ./pyenv/bin/activate && python3 ./levenshtein.py $(PORT) <free-ports | sort -n

.PHONY: clean
clean:
	rm -vf nmap-ports used-ports all-ports free-ports *~
