# Portfinder

Trivial script collection to find sympathic ports for new services. By default, it shows the tcp ports in [1025-65534] not mentioned in
the largest tcp port usage database (maintained by the nmap project).

Usage is trivial but patches are welcomed to improve docs.

It would be also useful to extend the script by giving the smallest free ports in the known smallest Löwenshtein-distance to another port
given in parameter.

Usage:

make -j free-ports

You probably want the "setop" script from here: https://gitlab.com/peter.horvath/maxxscripts/-/raw/master/bin/setop?ref_type=heads

A way to find the best free ports:

make levenshtein PORT=5432

...the result is a python dump. Feel free to find out, how can python print an array without re-sorting it.
