import sys
from Levenshtein import distance
from functools import cmp_to_key

def cmpres(item1, item2):
  if item1["dist"] < item2["dist"]:
    return -1
  elif item1["dist"] > item2["dist"]:
    return 1
  elif item1["port"] < item2["port"]:
    return -1
  elif item1["port"] > item2["port"]:
    return 1
  else:
    return 0

arr = []
for line in sys.stdin:
  port=line.strip()
  arr.append({'port': int(port), 'len': len(port), 'dist': distance(sys.argv[1], port)})

arr=sorted(arr, key=cmp_to_key(cmpres))
#print(repr(arr))
#for res in arr:
#for i in 0..(len(arr)-1):
#  print(repr(res))

print([repr(arr[i]) for i in range(0, len(arr)-1) ][0])

#for i in range(0, len(arr)-1):
#  print(repr(arr[i]))
